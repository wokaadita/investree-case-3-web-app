<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cookie;



class Controller extends BaseController
{

     public function getLocation($query){
        $client= new Client();
        $request=$client->request('GET',"https://www.metaweather.com/api/location/search/?query=$query",['proxy'=>'http://128.199.72.53:44321']);
        $response=$request->getBody()->getContents();

        $cityDetails=json_decode($response,true);
        
        for($i=0;$i<count($cityDetails);$i++){
            return $cityDetails[$i]['woeid'];
            echo"<br>";
        }

    }
    public function getForecast(Request $request){
        $client= new Client();
        $query=$request->input('search');
        $woeid=$this->getLocation($query);
        if ($woeid){
            $request=$client->request('GET',"https://www.metaweather.com/api/location/$woeid/",['proxy'=>'http://128.199.72.53:44321']);
            $response=$request->getBody()->getContents();
            $minutes=120;
            
            $weather=json_decode($response,true);
            $hasil=[];
            for($i=0;$i<count($weather['consolidated_weather']);$i++){
                $hasil[]=$weather['consolidated_weather'][$i];
            }
            return view('search',['city'=> $query,'weathers'=>$hasil]);
       
        }else{
            return view('search',['city'=> "City Not Found",'weathers'=>["Not Found"]]);
        }
    }
    
    public function getCookie(Request $request){
            $value = $request->cookie('new');
            // $response= new Response($value);
            // $response->withCookie(cookie('search',$value, 120));
            return view('welcome', ['last_search'=>$value]);
            // return $_COOKIE['new'];


    }

    public function autoDetect(Request $request){
       
        $domain= $_SERVER["REMOTE_ADDR"];
        $port=$_SERVER['REMOTE_PORT'];

        if ($domain!= null && $port!=null){
            $response_1=unserialize(
                file_get_contents("http://www.geoplugin.net/php.gp?ip=$domain:$port"));
            $city=$response_1['geoplugin_city'];
            $woeid=$this->getLocation($city);
            if ($woeid){
                $client= new Client();
                $request=$client->request('GET',"https://www.metaweather.com/api/location/$woeid/",['proxy'=>'http://128.199.72.53:44321']);
                $response_2=$request->getBody()->getContents();
                $weather=json_decode($response_2,true);
            
                $hasil=[];
                for($i=0;$i<count($weather['consolidated_weather']);$i++){
                    $hasil[]=$weather['consolidated_weather'][$i];
                }
        
                return view('search',['city'=> $city,'weathers'=>$hasil]);};
        }else{
            return view('search',['city'=> "City Not Found",'weathers'=>["Not Found"]]);
        }
 

    }

}
