How to run Web App Weather Forecast:
1. Open Terminal in directory "case-3-web-app-woka"
2. Run command "php -S localhost:8000 -t public" or "php artisan serve"
3. Open link using "ctrl+click"
4. Activate VPN/Proxy in Browser
5. Enter name of the city on search bar, every your last search will be saved when you back to Homepage
6. "Auto-Detect" Button can locate city where you accessed this web app  automatically without using search bar
7. Finish  